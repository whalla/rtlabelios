# RTLabel

[![CI Status](http://img.shields.io/travis/Marcin Gorny/RTLabel.svg?style=flat)](https://travis-ci.org/Marcin Gorny/RTLabel)
[![Version](https://img.shields.io/cocoapods/v/RTLabel.svg?style=flat)](http://cocoadocs.org/docsets/RTLabel)
[![License](https://img.shields.io/cocoapods/l/RTLabel.svg?style=flat)](http://cocoadocs.org/docsets/RTLabel)
[![Platform](https://img.shields.io/cocoapods/p/RTLabel.svg?style=flat)](http://cocoadocs.org/docsets/RTLabel)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RTLabel is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "RTLabel" , :git => 'git@bitbucket.org:whalla/rtlabelios.git'

## Author

Marcin Gorny, marcin.gorny@gmail.com

## License

RTLabel is available under the MIT license. See the LICENSE file for more info.

