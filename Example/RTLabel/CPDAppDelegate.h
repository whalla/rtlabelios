//
//  RTAppDelegate.h
//  RTLabel
//
//  Created by CocoaPods on 12/17/2014.
//  Copyright (c) 2014 Marcin Gorny. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
